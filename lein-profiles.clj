{:user {:plugins [; Emacs: required for CIDER REPL and clj-refactor
                  [cider/cider-nrepl "0.9.1"]
                  [refactor-nrepl "1.1.0"]
                 ]
        :dependencies [[alembic "0.3.2"]
                       [org.clojure/tools.nrepl "0.2.11"]]}}

